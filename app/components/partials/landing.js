'use strict';

angular.module('clientApp')
  .controller('LandingCtrl', LandingCtrl);

function LandingCtrl(SheetmusicService, MetaInformation) {
    MetaInformation.setTitle('PianoShelf - discover piano sheet music');
    MetaInformation.setDescription('Browse, share, and download piano sheet music for free.');

    /* jshint validthis: true */
    var vm = this;
    vm.selectSheetmusic = selectSheetmusic;

    function selectSheetmusic(displayingSheetmusic) {
        vm.displayingSheetmusic = displayingSheetmusic;
    }

    SheetmusicService.getSheetmusicList({order_by : 'popular', page_size : 6, page : 1})
    .success(function(data){

        vm.topSheetmusic = data.results;

        // get first sheetmusic in top
        var displayingSheetmusic = vm.topSheetmusic[0]; 

        vm.selectSheetmusic(displayingSheetmusic);
    });
}
