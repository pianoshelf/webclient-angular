'use strict';

angular.module('clientApp')
.controller('BodyController', BodyController);


function BodyController($scope, $state, $stateParams, $http, $modal, AlertService, AuthService, loginService) {

  $scope.isCollapsed = true;

  // for the add sheetmusic modal 
  $scope.openUploadSheemusicModal = function (size) {

    var modalInstance = $modal.open({
      templateUrl: 'components/upload/upload-sheetmusic-modal.html',
      controller: 'ModalInstanceCtrl',
      size: size,
    });
  };

  $scope.openUploadVideoModal = function () {

    var modalInstance = $modal.open({
      templateUrl: 'components/upload/upload-video-modal.html',
      controller: 'VideoModalInstanceCtrl',
      resolve: {
        id: function () {
          return '-';
        }
      }
    });

    modalInstance.result.then(function () {
    }, function () {
      // dismissed modal
    });
  };

  // Expose $state and $stateParams to the <body> tag
  $scope.$state = $state;
  $scope.$stateParams = $stateParams;

  // loginService exposed and a new Object containing login user/pwd
  $scope.ls = loginService;
  $scope.logout = function () {
    loginService.logoutUser(
      AuthService.logout()
      .success(function(){
          AlertService.addAlert('Successfully logged out.');
      })
    );
  };

  $scope.openLoginModal = function () {
    var modalInstance = $modal.open({
      templateUrl: '/components/auth/registerModal.html',
      controller: 'ModalController',
      backdropClass : '.backdrop',
      windowClass: 'login-modal-window'
    });
    $scope.currentModal = modalInstance;

    modalInstance.result.then(function () {
      $scope.isCollapsed = true;  // collapse navbar on close
     }, function () {
       $scope.isCollapsed = true; // collapse navbar on dismiss
    });
  };

  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){ 
    if ($scope.currentModal) {
      $scope.currentModal.dismiss();
    }
  });

}