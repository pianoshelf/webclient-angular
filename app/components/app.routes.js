'use strict';

/* global accessLevels:false */

angular.module('clientApp.routes', ['ui.router'])
.config(function ($stateProvider, $urlRouterProvider) {

  // redirect trailing slashes
 $urlRouterProvider.rule(function($injector, $location) {
    var path = $location.path();
    var hasTrailingSlash = path[path.length-1] === '/';

    if(hasTrailingSlash) {
      // return the same url without the slash  
      var newPath = path.substr(0, path.length - 1); 
      return newPath; 
    } 
  });

  $urlRouterProvider.otherwise('/');

  $stateProvider
    // abstract root state
    .state('app', {
      abstract: true,
      template: '<ui-view></ui-view>',
      resolve: {
        'login': function (loginService, $q, $http) {
          var roleDefined = $q.defer();

          /**
           * In case there is a pendingStateChange means the user requested a $state,
           * but we don't know yet user's userRole.
           *
           * Calling resolvePendingState makes the loginService retrieve his userRole remotely.
           */
          if (loginService.pendingStateChange) {
            return loginService.resolvePendingState($http.get('/api-auth/user/'));
          } else {
            roleDefined.resolve();
          }
          return roleDefined.promise;
        }
      }
    })
    // landing page
    .state('app.home', {
        url: '/',
        templateUrl: '/components/partials/landing.html',
        controller: 'LandingCtrl',
        controllerAs: 'landing',
        accessLevel: accessLevels.public
    })
    // Authentication
    .state('app.login', {
        url: '/login',
        templateUrl: '/components/auth/login.html',
        controller: 'LoginCtrl',
        accessLevel: accessLevels.public
    })
    .state('app.register', {
        url: '/register',
        templateUrl: '/components/auth/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'register',
        accessLevel: accessLevels.public
    })
    .state('app.verifyEmail', {
        url: '/verify-email/:key',
        templateUrl: '/components/auth/verify-email.html',
        controller: 'VerifyEmailCtrl',
        controllerAs: 'verifyemail',
        accessLevel: accessLevels.public
    })
    .state('app.searchresults', {
        url: '/search/:query',
        templateUrl: '/components/search/searchresult.html',
        controller: 'SearchResultCtrl',
    })
    // browse sheetmusic
    .state('app.browse', {
        url: '/sheetmusic',
        templateUrl: '/components/sheetmusic/views/browse.html',
        controller: 'BrowseCtrl',
        controllerAs: 'browse',
        abstract: true
    })
      .state('app.browse.sheetmusic',{
        url: '?order&page&view&asc',
        templateUrl: '/components/sheetmusic/views/browse.sheetmusic.html',
        controller: 'BrowseSheetmusicCtrl',
        controllerAs: 'browseSheetmusic'
      })
      .state('app.browse.composers', {
         url: '/composers',
         templateUrl: '/components/sheetmusic/views/browse.composers.html',
         controller: 'BrowseComposersCtrl',
         controllerAs: 'browseComposers'
      })

    .state('app.composer', {
        url: '/sheetmusic/:composer',
        templateUrl: '/components/sheetmusic/views/composer.html',
        controller: 'ComposerCtrl',
        controllerAs: 'composer'
    })
    .state('app.sheetmusic', {
        url: '/sheetmusic/:sheetmusicId/:slug',
        templateUrl: '/components/sheetmusic/views/sheetmusic.html',
        controller: 'SheetmusicDetailsCtrl',
    })

    // user profile
    .state('app.myshelf', {
        url: '/myshelf',
        templateUrl: '/components/profile/views/myshelf.html',
        controller: 'MyshelfCtrl',
        controllerAs: 'myshelf',
        accessLevel: accessLevels.user
    })
    .state('app.profile', {
        url: '/profile/:username',
        templateUrl: 'components/profile/views/profile.html',
        controller: 'ProfileCtrl',
        controllerAs: 'profile',
        abstract: true
    })
      .state('app.profile.shelf', {
          url: '',
          templateUrl: 'components/profile/views/profile.shelf.html',
          controller: 'ProfileShelfCtrl',
          controllerAs: 'profileShelf'
      })
      .state('app.profile.comments', {
          url: '/comments',
          templateUrl: 'components/profile/views/profile.comments.html',
          controller: 'ProfileCommentCtrl',
          controllerAs: 'profileComments'
      })
      .state('app.profile.uploads', {
          url: '/uploads',
          templateUrl: 'components/profile/views/profile.uploads.html',
          controller: 'ProfileUploadsCtrl',
          controllerAs: 'profileUploads'
      })
      .state('app.profile.videos', {
          url: '/videos',
          templateUrl: 'components/profile/views/profile.videos.html',
          controller: 'ProfileVideosCtrl',
          controllerAs: 'profileVideos'
      })

    // forum
    .state('app.forum', {
        url: '/forum',
        templateUrl: '/components/forum/forum.html',
        abstract: true
    })
    .state('app.forum.category', {
        url: '/category',
        templateUrl: '/components/forum/forum.category.html',
        controller: 'ForumCategoryCtrl'
    })
    .state('app.forum.topiclist', {
        url: '/topiclist?catTitle&catId',
        templateUrl: '/components/forum/forum.topiclist.html',
        controller: 'ForumTopicListCtrl'
    });
});
