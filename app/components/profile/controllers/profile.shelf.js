'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ProfileShelfCtrl
 * @description
 * # ProfileShelfCtrl
 * Controller of the clientApp
 */

angular.module('clientApp')
  .controller('ProfileShelfCtrl', ProfileShelfCtrl);

function ProfileShelfCtrl($scope, $stateParams, ShelfService, AlertService) {
    /* jshint validthis: true */
    var vm = this;
    var username = $stateParams.username;

    vm.profileUsername = username;
    vm.switchEditMode = switchEditMode;

    // get shelf for user
    ShelfService.getShelf(username)
    .success(function(data){
         vm.shelf = data.sheetmusic;
    });

    // emitted from the sheetmusic-list directive  
    $scope.$on('removeFromShelf', function(event, data){
      vm.shelf.splice(data.index, 1);

      ShelfService.removeFromShelf(data.sheetmusic.id)
      .success(function() {
        AlertService.addAlert('Successfully removed sheetmusic from shelf');
      });
    });

    function switchEditMode() {
        // let child scopes know that edit mode is turned on
        $scope.$broadcast('switchEditMode');
    }
}
