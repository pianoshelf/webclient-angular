'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ProfileCommentCtrl
 * @description
 * # ProfileCommentCtrl
 * Controller of the clientApp
 */
 
angular.module('clientApp')
  .controller('ProfileCommentCtrl', ProfileCommentCtrl);

function ProfileCommentCtrl($state, $stateParams, ProfileService) {
    /* jshint validthis: true */
    var vm = this;
    vm.username = $stateParams.username;

    ProfileService.getCommentsForUser(vm.username )
    .success(function(comments){
        vm.comments = comments;
    });
}
