'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ProfileVideosCtrl
 * @description
 * # ProfileVideosCtrl
 * Controller of the clientApp
 */

angular.module('clientApp')
  .controller('ProfileVideosCtrl', ProfileVideosCtrl);

function ProfileVideosCtrl($stateParams, $modal) {
  /* jshint validthis: true */
     var vm = this;

    var username = $stateParams.username;

    // open upload modal
    function openUploadVideoModal() {
      var modalInstance = $modal.open({
            templateUrl: 'components/upload/upload-video-modal.html',
            controller: 'VideoModalInstanceCtrl',
            resolve: {
              id: function () {
                return '-';
              }
            }
          });

      modalInstance.result.then(function () {
      }, function () {
      });
    }
}
