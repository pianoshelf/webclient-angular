'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ProfileCtrl', function ($state, $scope, $stateParams, $modal, ProfileService, AlertService) {

    /* jshint validthis: true */
    var vm = this,
        username = $stateParams.username,
        section = $stateParams.section;

    vm.editMode = false;
    vm.updateUser = updateUser;
    vm.switchEditMode = switchEditMode;
    vm.openProfilePictureModal = openProfilePictureModal;

    ProfileService.getProfile(username)
      .success(function (data) {
        vm.large_profile_picture = data.large_profile_picture;
        vm.full_name = data.full_name;
        vm.username = data.username;
        vm.description = data.description;
    });

    function openProfilePictureModal() {
      var modalInstance = $modal.open({
        templateUrl: 'components/upload/upload-profile-picture-modal.html',
        controller: 'ProfilePictureModalCtrl',
        resolve : {
          profilePicture: function() {
            return vm.large_profile_picture;
          }
        }
      });
    }

    function updateUser(){
      // TODO: Validate length
      ProfileService.updateProfileDescription(vm.description)
      .success(function(){
        AlertService.addAlert('Your profile description has been successfully updated.');
      });
    }

    function switchEditMode() {
      vm.editMode = !vm.editMode;
      var alertModeOnOff = vm.editMode ? 'on' : 'off';
    }

  });
