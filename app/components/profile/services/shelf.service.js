'use strict';

/**
 * @ngdoc service
 * @name clientApp.ShelfService
 * @description
 * # ShelfService
 * Service in the clientApp.
 */

angular.module('clientApp')
  .factory('ShelfService', ShelfService);

function ShelfService($http, $resource) {

    var service = {},
        shelfURL = '/api/shelf/';

    service.getShelf = function(username) {
        return $http({
            method: 'GET',
            url: shelfURL,
            params : {username : username}
        });
    };

    service.addToShelf = function(sheetId) {
        return $http.post(shelfURL, {sheetmusic : sheetId});
    };

    service.removeFromShelf = function(sheetId) {
        return $http.delete(shelfURL + '?sheetmusic=' + sheetId);
    };

    return service;
}
