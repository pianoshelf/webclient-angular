'use strict';

/**
 * @ngdoc service
 * @name clientApp.ProfileService
 * @description
 * # ProfileService
 * Service in the clientApp.
 */

angular.module('clientApp')
  .factory('ProfileService', ProfileService);

function ProfileService($http, $resource) {

    var service = {},
        profileURL = '/api/profile/';

    service.getProfile = function(username) {
        return $http.get( profileURL + '?username=' + username );
    };

    service.updateProfileDescription = function(description) {
        return $http.post( profileURL, {description: description} );
    };

    service.getCommentsForUser = function(username) {
        return $http.get('/api/comment/' + '?username=' + username);
    };

    service.getUploadsForUser = function(username, pageNumber) {
        return $http.get('/api/sheetmusic/uploads/?username=' + username + '&page=' + pageNumber);
    };

    service.getVideosForUser = function(username) {
        return $http.get('/api/video/' + '?username=' + username);
    };

    return service;
}
