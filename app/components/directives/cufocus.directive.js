'use strict';

/**
 * @ngdoc directive
 * @name clientApp.directive:cuFocus
 * @description
 * # cuFocus
 */
angular.module('clientApp')
  .directive('cuFocus', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function postLink(scope, element, attrs, controller) {

          controller.$focused = false;
          element.bind('focus', function(e){
              scope.$apply(function(){
                 controller.$focused = true;
              });
          }).bind('blur', function(e){
             scope.$apply(function() {
                 controller.$focused = false;
             });
          });
      }
    };
  });
