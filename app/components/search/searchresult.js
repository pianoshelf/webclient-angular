'use strict';

angular.module('clientApp')
.controller('SearchResultCtrl', function($scope, $stateParams, SheetmusicService) {
    var query = $stateParams.query;
    SheetmusicService.search(query)
    .success(function(data){
        $scope.results = data;
    });
});
