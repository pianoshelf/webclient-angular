'use strict';

angular.module('clientApp')
.controller('TypeaheadCtrl', function($scope, $state, $http, SheetmusicService) {
  
  $scope.selectSearchResult = function($item, $model) {
    var params = {'sheetmusicId' : $item.id, 'slug' : $item.uniqueurl};
    $state.go('app.sheetmusic', params);
    //$scope.searchText = "";
    $model.blur();
  };

  $scope.getSheetmusic = function(query) {
    return SheetmusicService.search(query)
    .then(function(response){
      return response.data.free;
    });
  };
  
  $scope.searchSubmit = function(query) {
    $state.go('app.searchresults', {'query' : query});
  };
});
