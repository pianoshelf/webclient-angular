'use strict';

function ForumCategoryCtrl($scope, $http, $stateParams, $sce, AlertService, ForumService) {

    $scope.createTopic = createTopic;
    $scope.createPost = createPost;

    ForumService.getCategories().success(function(data) {
      $scope.categories = data;
    });

    function createTopic(){
    
        if(!$scope.ls.isLogged){
            AlertService.addAlert('Log in to contribute to the forums');
            return;
          }

          var title = 'DARK';
          var body = 'PARK';
          var categoryId = '4';

        ForumService.createTopic(title, body, categoryId)
        .success(function(data){
               AlertService.addAlert('success boys');
        })
        .error(function(data){
               AlertService.addAlert('Ruh Roh');
               console.log(data);
        });
    }

    function createPost(){
    
        if(!$scope.ls.isLogged){
            AlertService.addAlert('Log in to contribute to the forums');
            return;
          }

          var body = 'PARK';
          var topicId = '1';
          var recipientId = '';

        ForumService.createPost(body, topicId, recipientId)
        .success(function(data){
               AlertService.addAlert('success boys II');
        })
        .error(function(data){
               AlertService.addAlert('Ruh Roh II');
               console.log(data);
        });
    }

}

angular.module('clientApp')
  .controller('ForumCategoryCtrl', ForumCategoryCtrl);
