'use strict';

function ForumTopicListCtrl($scope, $state, $http, $stateParams, $sce, ForumService) {

    $scope.categoryTitle = $state.params.catTitle;
    var categoryId = $state.params.catId;

    var sort = 'views';

    ForumService.getTopics(sort, categoryId).success(function(data) {
        $scope.topics = data;
        console.log(data);
    });

}

angular.module('clientApp')
  .controller('ForumTopicListCtrl', ForumTopicListCtrl);
