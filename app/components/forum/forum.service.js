'use strict';

/**
 * @ngdoc service
 * @name clientApp.ForumService
 * @description
 * # ForumService
 * Service in the clientApp.
 */

angular.module('clientApp')
  .factory('ForumService', ForumService);

function ForumService($http, $resource) {

    var service = {},
        forumURL = '/api/forum/';

    service.getCategories = function(){
        return $http.get(forumURL + 'categories');
    };

    service.getTopics = function(sort, category){
        return $http.get(forumURL + 'topics/?sort=' + sort + '&category=' + category);
    };

    service.getPosts = function(topic){
        return $http.get(forumURL + 'posts/?topic=' + topic);
    };

    service.createTopic = function(title, body, categoryId) {
        return $http.post(forumURL + 'topics', {
          category_id: categoryId,
          title: title,
          body: body
        });
    };

    service.createPost = function(body, topicId, recipientId) {
        return $http.post(forumURL + 'posts', {
          recipient_id: recipientId,
          topic_id: topicId,
          body: body
        });
    };

    return service;
}
