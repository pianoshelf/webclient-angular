'use strict';

/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 * Try to avoid putting controller logic here
 */

angular.module('clientApp', [
        // login service
        'loginService',

        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'ui.bootstrap',
        'headroom',
        'duScroll',

        'angularFileUpload',
        'angular-loading-bar',
        'xeditable',
        'angulartics', 
        'angulartics.google.analytics',

        // root state
        'clientApp.routes'
    ])
.config(function ($stateProvider, $urlRouterProvider, $locationProvider, cfpLoadingBarProvider) {
    $locationProvider.html5Mode(true);
    cfpLoadingBarProvider.includeSpinner = false;
})
.run(function ($rootScope, $state, editableOptions, MetaInformation, loginService) {
  editableOptions.theme = 'bs3';

  // Make MetaInformation globally accessible
  $rootScope.MetaInformation = MetaInformation;

  /**
   * $rootScope.doingResolve is a flag useful to display a spinner on changing states.
   * Some states may require remote data so it will take awhile to load.
   */
  var resolveDone = function (event, next, current) { 
    $rootScope.doingResolve = false; 
    if (loginService.user.username !== undefined && next.templateUrl === '/components/partials/landing.html') {
      $state.go('app.profile.shelf', {username: loginService.user.username});
    }
  };

  $rootScope.doingResolve = false;
  $rootScope.$on('$stateChangeStart', function () {
    $rootScope.doingResolve = true;
  });
  $rootScope.$on('$stateChangeSuccess', resolveDone);
  $rootScope.$on('$stateChangeError', resolveDone);
  $rootScope.$on('$statePermissionError', resolveDone);

  // Global Alerts for actions, TODO: make into directive
  $rootScope.alerts = [
    //{ type: 'alert-success', msg: 'Alert Message'}
  ];
})
.controller('NavCtrl', function($scope, $location) {
  $scope.isActive = function(route) {
    $scope.path = $location.path();
    return $location.path() === route;
  };
})
.controller('ModalController',function($scope, $modalInstance) {
    $scope.closeLoginModal = function () {
        $modalInstance.close();
    };
})
.factory('httpRequestInterceptor', function ($cookies) {
  return {
    request: function (config) {
      config.headers['X-CSRFToken'] = $cookies.csrftoken;
      return config;
    }
  };
})
.config(function ($httpProvider) {
  $httpProvider.interceptors.push('httpRequestInterceptor');
  $httpProvider.defaults.headers.patch = {
      'Content-Type': 'application/json;charset=utf-8'
  };
});
