'use strict';

angular.module('clientApp')
.controller('VideoModalInstanceCtrl', function ($scope, $modalInstance, SheetmusicService, AlertService, id) {

  $scope.grade = '';
  $scope.id = id;

  $scope.status = {
    isopen: false
  };

  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.submit = function() {
    
    var url = $scope.sheetmusic.url;
    var title = $scope.sheetmusic.title;
    var grade = $scope.grade.toLowerCase();
    var id = $scope.id;

    SheetmusicService.postVideo(url, id, title, grade)
    .success(function(){
      AlertService.addAlert('Successfully added video!');
    })
    .error(function(){
      AlertService.addAlert('There was an error adding the video!');
    });
    $modalInstance.close();
  };

});