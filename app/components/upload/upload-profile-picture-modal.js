'use strict';

angular.module('clientApp')
.controller('ProfilePictureModalCtrl', function ($scope, $modalInstance, $upload, AlertService, profilePicture) {

  $scope.profilePicture = profilePicture;
  $scope.fileSizeExceeded = false;

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.onFileSelect = function($file) {
    $scope.file = $file[0];

    // greater than 10MB
    if ($scope.file && $scope.file.size > 10000000) {
      $scope.fileSizeExceeded = true;      
    } else {
      $scope.fileSizeExceeded = false;
    }
  };

  $scope.submit = function() {
    var file = $scope.file;

    if (!file) {
      console.log('no file');
    }
    else {
      $upload.upload({
        url: '/api/submit/profilepicture/', 
        method: 'POST',
        file: file,
      })
      .success(function() {
        // file is uploaded successfully
        $modalInstance.close();
        AlertService.addAlert('Profile picture successfully saved. It will be updated momentarily.');

      }).error(function(err){
        
        AlertService.addAlert('Error sending profile picture');
      });  
    }
  };
});
