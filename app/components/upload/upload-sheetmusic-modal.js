'use strict';

angular.module('clientApp')
.controller('ModalInstanceCtrl', function ($scope, $state, $modalInstance, $upload, AlertService) {

  $scope.fileSizeExceeded = false;
  $scope.file = null;

  $scope.onFileSelect = function($file) {
    $scope.file = $file[0];

    // greated than 10MB
    if ($scope.file && $scope.file.size > 10000000) {
      $scope.fileSizeExceeded = true;      
    } else {
      $scope.fileSizeExceeded = false;
    }
  };

  $scope.upload = function () {

    var sheetmusic = $scope.sheetmusic;
    var file = $scope.file;

    if (!file) {
      console.log('no file');
    }
    else {
      $upload.upload({
        url: '/api/submit/sheetmusic/', 
        method: 'POST',
        data: {sheetmusic: sheetmusic},
        file: file,
      })
      .success(function(data) {
        // file is uploaded successfully
        $modalInstance.close();
        AlertService.addAlert('Sheetmusic successfully uploaded');
        $state.go('app.sheetmusic',{sheetmusicId: data.id, slug: data.uniqueurl});
      }).error(function(err){
        console.log(err);
      });  
    }

  };

  $scope.cancel = function () {

    $modalInstance.dismiss('cancel');
  };

});