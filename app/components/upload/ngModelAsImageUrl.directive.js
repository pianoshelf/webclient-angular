'use strict';

/*
  uses fileReader to set the ngModel as the image data url
*/

angular.module('clientApp')
  .directive('ngModelAsImageUrl', function(fileReader, $timeout) {
    return {
      replace: false,
      require: 'ngModel',
      link: function(scope, el, attrs, ngModel) {

        function getFile(file) {
          fileReader.readAsDataUrl(file, scope)
            .then(function(result) {
                ngModel.$setViewValue(result);
            });
        }
        el.bind('change', function(e) {
          var file = (e.srcElement || e.target).files[0];
          getFile(file);
        });
      
      }
    };
  });
