'use strict';

angular.module('clientApp')
  .directive('pianoSheetmusicList', function () {
      
      return {
          restrict: 'E',
          templateUrl: 'components/sheetmusic/directives/sheetmusic-list.directive.html',
        scope: { 
            //@ reads the attribute value, = provides two-way binding, & works with functions
            sheetmusics: '=',
          },
        link: function (scope, el, iAttrs) {
            scope.editMode = false;
            scope.removeFromShelf = removeFromShelf;

            scope.$on('switchEditMode', function(event) {
                scope.editMode = !scope.editMode;
            });

            function removeFromShelf(sheet, index){
                scope.$emit('removeFromShelf', {sheetmusic: sheet, index: index});
            }
        }
      };
});
