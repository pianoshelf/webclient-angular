'use strict';

angular.module('clientApp')
  .directive('pianoVideo', function (SheetmusicService, ProfileService, $modal, $sce, MetaInformation) {
    return {
      restrict: 'E',
      templateUrl: 'components/sheetmusic/directives/video.html',
      scope: {
        //@ reads the attribute value, = provides two-way binding, & works with functions
        username: '=',
        sheetmusic: '=',
      },
      link: function(scope, element, attrs, controller) {

        scope.selectVideo = selectVideo;
        scope.reset = reset;
        scope.professionalactive = false;

        // Sheetmusic video
        if (scope.username === false){
          scope.$watch('sheetmusic', function(data) {
              if(data) {
                var professional = [];
                var amateur = [];

                // add videos to scope
                var videos = data.videos.map(function(video){
                  video.url = $sce.trustAsResourceUrl('//www.youtube.com/embed/' + video.youtube_id);
                  return video;
                });

                for(var item in videos){
                  if (videos[item].grade === 'professional'){
                    professional.push(videos[item]);
                  }
                  else if(videos[item].grade === 'amateur'){
                    amateur.push(videos[item]);
                  }
                }

                scope.professional_videos = professional;
                scope.amateur_videos = amateur;
                scope.selectedIdx = 0;
                scope.professionalactive = scope.professional_videos.length || !scope.amateur_videos.length;
              }
          });
        }
        // Profile video
        else {
          scope.$watch('username', function(data) {
            if(data){
              ProfileService.getVideosForUser(scope.username).success(function(data) {
                var professional = [];
                var amateur = [];

                // add videos to scope
                var videos = data.map(function(video){
                video.url = $sce.trustAsResourceUrl('//www.youtube.com/embed/' + video.youtube_id);
                return video;
                });

                for(var item in videos){
                  if (videos[item].grade === 'professional'){
                    professional.push(videos[item]);
                  }
                  else if(videos[item].grade === 'amateur'){
                    amateur.push(videos[item]);
                  }
                }

                scope.professional_videos = professional;
                scope.amateur_videos = amateur;
                scope.selectedIdx = 0;
                scope.professionalactive = scope.professional_videos.length || !scope.amateur_videos.length;
              });
            }
          });
        }

        // select a video
        function selectVideo(index) {
          scope.selectedIdx = index;
        }

        function reset(){
          scope.selectedIdx = 0;
        }

      }

  };

});
