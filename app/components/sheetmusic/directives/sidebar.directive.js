'use strict';

angular.module('clientApp')
  .directive('pianoSidebar', function ($window) {
    return {
      restrict: 'E',
      scope: {
        //@ reads the attribute value, = provides two-way binding, & works with functions
        sheetmusic: '=',
      },
      templateUrl: 'components/sheetmusic/directives/sidebar.html',
      link: function(scope, element, attrs, controller) { 

        var initial_position = $window.pageYOffset;

        function offset(){
          var new_position = $window.pageYOffset;
          if (new_position === initial_position){
            return;
          }
          else if (new_position > initial_position){
            $('#AnchorList').stop().animate({marginTop: -1*Math.min(new_position, $('nav').height())}, 200);
          }
          else{
            $('#AnchorList').stop().animate({marginTop: 0}, 200);
          }
          initial_position = new_position;
        }

        offset();
        angular.element($window).bind('scroll', function() {
          offset();
        });

        scope.$watch('sheetmusic', function(data) {
          if(data){
            scope.video_length = data.videos.length;
            scope.comments_length = data.comments.length;
          }
        });
      }
  };
});
