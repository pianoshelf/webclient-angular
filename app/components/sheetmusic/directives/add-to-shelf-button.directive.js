'use strict';

angular.module('clientApp')
  .directive('pianoAddToShelfButton', function (ShelfService, AlertService) {
      
      return {
          restrict: 'E',
          templateUrl: 'components/sheetmusic/directives/add-to-shelf-button.html',
        scope: { 
            //@ reads the attribute value, = provides two-way binding, & works with functions
            sheetmusic: '=sheetmusic',
            sheetmusicId: '=sheetmusicid',
            sheetmusicTitle: '=title'
          },
        link: function (scope, el, attrs) {
            scope.shelfAction = shelfAction;

            function shelfAction(){
              if (!scope.$parent.ls.isLogged) {
                $('#shelfAction').popover('show');
                return;
              }

              if (scope.isInShelf){
                ShelfService.removeFromShelf(scope.sheetmusicId)
                .success(function(data){
                  scope.isInShelf = false;
                  AlertService.addAlert('Removed ' + scope.sheetmusicTitle + ' ' + 'from shelf');
                })
                .error(function(data){
                });
              }
              else {
                ShelfService.addToShelf(scope.sheetmusicId)
                .success(function(data){
                  scope.isInShelf = true;
                  AlertService.addAlert('Added ' + scope.sheetmusicTitle + ' ' + ' to shelf');
                })
                .error(function(data){
                });
              }
            }


            $('#shelfAction').popover({
              content: '<b>Log in</b> to add sheetmusic to your shelf.',
              html: true,
              placement: 'bottom',
              trigger: 'manual'
            });
        }
      };
});
