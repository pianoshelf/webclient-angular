'use strict';

angular.module('clientApp')
  .directive('pianoUploadList', function ($modal) {
      
      return {
        restrict: 'E',
        templateUrl: 'components/sheetmusic/directives/upload-list.html',
        scope: false,
        controllerAs: 'uploadList',
        controller : function($scope, ProfileService) {
          var vm = this;

          vm.paginationPage = 1;
          
          vm.updateProfileUploads = updateProfileUploads;
          vm.deleteSheetmusic = deleteSheetmusic;

          updateProfileUploads();

          function updateProfileUploads() {
            // get shelf for user
            ProfileService.getUploadsForUser($scope.profileShelf.profileUsername, vm.paginationPage)
            .success(function(data){
                vm.uploads = data.results;
                vm.totalItems = data.count;
            });  
          }

          function deleteSheetmusic(sheetmusic, idx) {

              var modalInstance = $modal.open({
                templateUrl: 'components/partials/delete-confirm-modal.html',
                controller: 'DeleteConfirmCtrl',
                resolve: {
                  sheetmusic: function () {
                    return sheetmusic;
                  }
                }
              });

              modalInstance.result.then(function () {
                    vm.uploads.splice(idx, 1);    
              });
          }
        }
      };
});

angular.module('clientApp')
  .controller('DeleteConfirmCtrl', DeleteConfirmCtrl);

function DeleteConfirmCtrl($scope, $modalInstance, AlertService, SheetmusicService, sheetmusic) {

  $scope.delete = function () {
    // DELETE THE SHEETMUSIC

    SheetmusicService.deleteSheetmusic(sheetmusic.id)
    .success(function(){
      AlertService.addAlert('Successfully deleted sheetmusic.');
      $modalInstance.close();
    })
    .error(function(err){
      AlertService.addAlert('There was an error deleting the file. Please try again.');
    });
    
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  };
}