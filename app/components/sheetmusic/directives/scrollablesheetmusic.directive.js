'use strict';

/* global SwipeView:false */

angular.module('clientApp')
  .directive('scrollableSheetmusic', function () {
    return {
      restrict: 'E',
      templateUrl: 'components/sheetmusic/directives/scrollablesheetmusic.html',
      scope: {
        //@ reads the attribute value, = provides two-way binding, & works with functions
        sheetmusic: '=',
      },
      link: function(scope, element, attrs, controller) { 

        scope.$watch('sheetmusic', function() { 
          if (scope.sheetmusic !== undefined) {

            if(scope.sheetmusic.upload_complete) {

              // reset
              $('#swipeview-slider').remove();

              // Documentation: http://cubiq.org/swipeview
              var carousel,
                el,
                i,
                page,
                slides = scope.sheetmusic.images;

              // update page number 
              $('#total_pages').text(slides.length);

              carousel = new SwipeView('#img_container', {
                numberOfPages: slides.length,
                hastyPageFlip: true
              });

              // Load initial data
              for (i=0; i<3; i++) {
                page = i===0 ? slides.length-1 : i-1;
                el = document.createElement('img');
                el.src = slides[page];
                //el.alt = slides[page].alt;
                el.className = 'img-responsive';
                el.draggable = false;
                carousel.masterPages[i].appendChild(el);
              }

              carousel.onFlip(function () {
                  $('#swipe-tip').hide();
                var el,
                  upcoming,
                  i;

                for (i=0; i<3; i++) {
                  upcoming = carousel.masterPages[i].dataset.upcomingPageIndex;

                  if (upcoming !== carousel.masterPages[i].dataset.pageIndex) {
                    el = carousel.masterPages[i].querySelector('img');
                    el.src = slides[upcoming];
                    //el.alt = slides[page].alt;
                    el.className = 'img-responsive';
                    el.draggable = false;
                    $('#current_page').text(carousel.pageIndex+1);
                  }
                }
              });
              // Right and left buttons to switch page 
              $('#leftbar').click(function(){ carousel.prev(); });
              $('#rightbar').click(function(){ carousel.next(); });
            }
            else {
              // Sheetmusic is being generated
              $('#img_container').hide();
              $('#page_number').hide();
              $('#sheetmusic_not_ready').show();
            }

          }
        });

      }
    };
  });
