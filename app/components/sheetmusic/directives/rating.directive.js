'use strict';

angular.module('clientApp')
  .directive('pianoRating', function ($timeout, SheetmusicService, AlertService) {
    return {
      restrict: 'E',
      templateUrl: 'components/sheetmusic/directives/rating.html',
      scope: {
        //@ reads the attribute value, = provides two-way binding, & works with functions
        sheetmusicid: '=',
        ratingval: '=ratingval'
      },
      link: function(scope, element, attrs, controller) { 

        scope.rate = 0;
        scope.max = 5;

        scope.hoveringOver = hoveringOver;
        scope.hoverOff = hoverOff;
        scope.ratingSubmit = ratingSubmit;
        
        var prev_rate;

        scope.$watch('ratingval', function(ratingval) {

          if (ratingval !== undefined) {
            scope.rate = ratingval;
            scope.val = scope.rate;
            prev_rate = ratingval;
            set_label(ratingval);  
          }
        });

        function set_label(value){
          switch(value) {
            case 0:
              scope.ratinglabel = 'No Rating';
              break;
            case 1:
              scope.ratinglabel = 'Beginner';
              break;
            case 2:
              scope.ratinglabel = 'Novice';
              break;
            case 3:
              scope.ratinglabel = 'Intermediate';
              break;
            case 4:
              scope.ratinglabel = 'Advanced';
              break;
            case 5:
              scope.ratinglabel = 'Expert';
              break;   
          }
        }

        function hoveringOver(value) {
          scope.overStar = value;
          scope.val = value;
          set_label(scope.val);
        }

        function hoverOff() {
          scope.overStar = null;
          scope.val = prev_rate;
          set_label(prev_rate);
        }

        scope.ratingStates = [
          {stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty'},
        ];

        function ratingSubmit(){

          SheetmusicService.postRating(scope.sheetmusicid, scope.rate)
          .success(function(data){
              prev_rate = scope.rate;
              scope.message ='Thanks for rating!';
              AlertService.addAlert('Thanks for rating!');
              $timeout(function(){
                SheetmusicService.getRating(scope.sheetmusicid)
                .success(function(data){
                    scope.rate = data.value;
                    scope.val = scope.rate;
                    prev_rate = data.value;
                    set_label(data.value);  
                  })
                  .error(function(data){
                    scope.rate = scope.rate;
                    prev_rate = data.value;
                });
                scope.message='';
                scope.$apply();
              }, 2500);
            })
            .error(function(data){
              scope.rate = prev_rate;
              AlertService.addAlert('Please login to rate the piece.');
          });
        }

      }
    };
  });
