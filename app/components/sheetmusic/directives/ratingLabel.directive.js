'use strict';

angular.module('clientApp')
  .directive('ratingLabel', function ($timeout, SheetmusicService, AlertService) {
    return {
      restrict: 'E',
      templateUrl: 'components/sheetmusic/directives/ratingLabel.html',
      scope: {
        ratingVal: '=ratingval',
      },
      link: function(scope, element, attrs, controller) {

        scope.$watch('ratingVal', function(ratingVal) {

          if (ratingVal !== undefined) {
            set_label(ratingVal);
          }
        });

        function set_label(value){
          switch(value) {
            case 0:
              scope.ratinglabel = 'No Rating';
              break;
            case 1:
              scope.ratinglabel = 'Beginner';
              break;
            case 2:
              scope.ratinglabel = 'Novice';
              break;
            case 3:
              scope.ratinglabel = 'Intermediate';
              break;
            case 4:
              scope.ratinglabel = 'Advanced';
              break;
            case 5:
              scope.ratinglabel = 'Expert';
              break;   
          }
        }
      }
    };
  });
