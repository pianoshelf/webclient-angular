
/**
 * @ngdoc function
 * @name clientApp.controller:SheetmusicDetailsCtrl
 * @description
 * # SheetmusicDetailsCtrl
 * Controller of the clientApp
 */

angular.module('clientApp')
  .controller('SheetmusicDetailsCtrl', SheetmusicDetailsCtrl);

function SheetmusicDetailsCtrl($scope, $http, $stateParams, $location, $sce, $modal, MetaInformation, AlertService, SheetmusicService, ShelfService, $window) {

    $scope.buttonComment = true;
    $scope.updateDownloads = updateDownloads;
    $scope.openUploadVideoModal = openUploadVideoModal;

    $scope.sortComment = sortComment;
    $scope.upvoteComment = upvoteComment;
    $scope.submitComment = submitComment;
    $scope.removeComment = removeComment;
    $scope.undoRemoveComment = undoRemoveComment;
    $scope.editComment = editComment;
    $scope.saveSheetmusic = saveSheetmusic;

    $scope.sheetmusicid = sheetmusicId;
    $scope.removecomment = false;

    var sheetmusicId = $stateParams.sheetmusicId;

    SheetmusicService.getSheetmusic(sheetmusicId).success(function(data) {
      var lIdx = $location.path().lastIndexOf('/');
      var slug = $location.path().substring( lIdx + 1 );
      if (slug !== data.uniqueurl) {
        $location.path( $location.path().substring(0, lIdx + 1) + data.uniqueurl);
      }

      // add sheetmusic images to scope
      $scope.sheetmusic = data; 

      MetaInformation.setTitle($scope.sheetmusic.short_description);
      MetaInformation.setDescription($scope.sheetmusic.long_description);
    });

    function saveSheetmusic(data) {
      SheetmusicService.updateSheetmusic(sheetmusicId, data)
      .success(function(resp){
        AlertService.addAlert('The sheetmusic ' + $scope.sheetmusic.title + ' has been successfully updated.');
      })
      .error(function(resp){
        AlertService.addAlert('There was an error updating the sheetmusic! Please try again.');
      });
    }

    function updateDownloads() {
      // update download count
      $http.post('/api/sheetmusic/downloads/', {'sheetmusic_id' : sheetmusicId});
    }

    // open upload modal
    function openUploadVideoModal() {
        var modalInstance = $modal.open({
          templateUrl: 'components/upload/upload-video-modal.html',
          controller: 'VideoModalInstanceCtrl',
          resolve: {
            id: function () {
              return sheetmusicId;
            }
          }
        });

        modalInstance.result.then(function () {

        }, function () {
        });
     }

    // ====== COMMENTING ========
    // TODO : Separate comments into a directive

    SheetmusicService.getComments(sheetmusicId).success(function(data) {
      $scope.comments = data;
      $scope.sortType = 'upvotes';
      $scope.sortSequence = true;
      $scope.sortText = 'Most Upvoted';
    });

    function sortComment(sortType, sortSequence, sortText) {
      $scope.sortType = sortType;
      $scope.sortSequence = sortSequence;
      $scope.sortText = sortText;
    }

    function submitComment(recipientId, text){
      if(!$scope.ls.isLogged){
        AlertService.addAlert('Log in to comment on this sheetmusic.');
        return;
      }

      if(text.trim() === ''){
        return;
      }
      
      SheetmusicService.addComment(text, sheetmusicId, recipientId)
      .success(function(data){
        // whoa.. updating the ENTIRE comments!
        $scope.comments = data;
        $scope.sortType = 'date';
        $scope.sortSequence = true;
        $scope.sortText = 'Most Recent';
      })
      .error(function(data){
      });
    }

    function upvoteComment(comment, commentId){
      if(!$scope.ls.isLogged){
        AlertService.addAlert('Log in to upvote this comment.');
        return;
      }

      SheetmusicService.upvoteComment(commentId, sheetmusicId)
      .success(function(){
        comment.upvotes += 1;
      })
      .error(function(data){
        AlertService.addAlert('You have already upvoted this comment.');
      });
    }

    function removeComment(commentId){
      SheetmusicService.removeComment(commentId)
      .success(function(){
        SheetmusicService.getComments(sheetmusicId).success(function(data) {
          $scope.comments = data;
          $scope.sortType = 'date';
          $scope.sortSequence = true;
          $scope.sortText = 'Most Recent';
          AlertService.addAlert('Comment removed.');
        });
      })
      .error(function(data){
        AlertService.addAlert('Comment could not be removed.');
      });
    }

    function undoRemoveComment(commentId){
      SheetmusicService.undoRemoveComment(commentId)
      .success(function(){
        SheetmusicService.getComments(sheetmusicId).success(function(data) {
          $scope.comments = data;
          $scope.sortType = 'date';
          $scope.sortSequence = true;
          $scope.sortText = 'Most Recent';
          AlertService.addAlert('Comment restored.');
        });
      })
      .error(function(data){
        AlertService.addAlert('Comment could not be restored.');
      });
    }


    function editComment(commentId, commentText){
      SheetmusicService.editComment(commentId, commentText)
      .success(function(){
        SheetmusicService.getComments(sheetmusicId).success(function(data) {
          $scope.comments = data;
          $scope.sortType = 'date';
          $scope.sortSequence = true;
          $scope.sortText = 'Most Recent';
          AlertService.addAlert('Comment edited');
        });
      })
      .error(function(data){
        AlertService.addAlert('Comment could not edited.');
      });
    }
}
