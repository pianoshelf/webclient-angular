'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:CompositionsCtrl
 * @description
 * # CompositionsCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
  .controller('ComposerCtrl', function ($stateParams, SheetmusicService, MetaInformation) {

    var vm = this;

    // get lists of sheetmusic by the selected composer
    vm.composer = $stateParams.composer;

    MetaInformation.setTitle(vm.composer + ' piano sheet music.');
    MetaInformation.setDescription('A list of free sheet music composed by ' + vm.composer + ' for piano');

    SheetmusicService.getSheetmusicList({composer_name: vm.composer, page_size : 200}).success(function(data){
    vm.compositions = data.results;
    });
});
