'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:BrowseSheetmusicCtrl
 * @description
 * # BrowseSheetmusicCtrl
 * Controller of the clientApp
 */

angular.module('clientApp')
  .controller('BrowseSheetmusicCtrl', BrowseSheetmusicCtrl);

function BrowseSheetmusicCtrl($state, $scope, MetaInformation, SheetmusicService, $timeout) {
    MetaInformation.setTitle('Pianoshelf - Browse Sheetmusic');
    MetaInformation.setDescription('Browse piano sheetmusic.');

    /* jshint validthis: true */
    var vm = this;

    vm.totalItems = 9;
    vm.order = $state.params.order === undefined ? 'popular' : $state.params.order;
    vm.ascendingOrder = false;
    if ($state.params.asc === 'true') {
        vm.ascendingOrder = true;
    }
    vm.viewType = $state.params.view;
    vm.pageNumber = $state.params.page;
    vm.isListView = isListView;
    vm.switchView = switchView;

    vm.pageChanged = pageChanged;
    vm.orderAscending = orderAscending;
    
    updateSheetmusicList(vm.ascendingOrder, vm.order, vm.pageNumber);

    function orderAscending(order) {
        vm.ascendingOrder = order;
        $state.transitionTo('app.browse.sheetmusic', {page: vm.paginationPage, order: vm.order, view: vm.viewType, asc: vm.ascendingOrder});
        //updateSheetmusicList(vm.ascendingOrder, vm.order, vm.pageNumber);
    }

    function updateSheetmusicList(is_ascending, order, page_num) {

        // trending sheetmusic is on a separate endpoint
        if (order === 'trending') {
            // trending for the past week
            SheetmusicService.getTrendingSheetmusic(7, vm.totalItems)
            .success(function(data){
                // since the format of trending downloads is in a different format
                // { 
                //    'sheetmusic': {...}, 
                //    'downloads' : 123
                // } 
                // where downloads is the number for the specified time period
                var filteredResults = data.map(function(result){
                    var sheetmusicDetails = result.sheetmusic;
                    // append the number of downloads for the week
                    sheetmusicDetails.downloads = result.downloads;
                    return sheetmusicDetails;
                });

                vm.sheetmusicResults = filteredResults;
            })
            .error(function(err){
                console.log(err);
            });
        }
        // either 'popular', 'new', or 'difficulty'
        else {
            var sort_by = is_ascending ? 'asc' : 'desc';

            SheetmusicService.getSheetmusicList({order_by : order, sort_by: sort_by, page_size : 9, page : page_num})
            .success(function(data) {
                vm.totalItems = data.count; 
                vm.sheetmusicResults = data.results;

                // set to the page from url params initially
                vm.paginationPage = vm.pageNumber;
            });
        }
    }

    function isListView() {
        // listview by default
        return vm.viewType === undefined ? true:  (vm.viewType === 'thumb' ? false : true);
    }

    function switchView () {
        if (vm.isListView()) {
            vm.viewType = 'thumb';
        } else {
            vm.viewType = undefined;
        }
        vm.listView = !vm.listView;
    }

    function pageChanged() {
        $state.transitionTo('app.browse.sheetmusic', {page: vm.paginationPage, order: vm.order, view: vm.viewType, asc: vm.ascendingOrder});
    }
}
