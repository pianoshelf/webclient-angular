'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:BrowseCtrl
 * @description
 * # BrowseCtrl
 * Controller of the clientApp
 */

angular.module('clientApp')
  .controller('BrowseCtrl', BrowseCtrl);

function BrowseCtrl(SheetmusicService) {
    /* jshint validthis: true */
    var vm = this;

    vm.searching = false;
    vm.searchTextChanged = searchTextChanged;

    function searchTextChanged() {
        SheetmusicService.search(vm.searchText)
        .success(function(data){
            vm.searchresults = data;
            console.log(data);
        });
    }
}
