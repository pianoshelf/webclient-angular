'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:BrowseComposersCtrl
 * @description
 * # BrowseComposersCtrl
 * Controller of the clientApp
 */

angular.module('clientApp')
  .controller('BrowseComposersCtrl', BrowseComposersCtrl);

function BrowseComposersCtrl(SheetmusicService) {
    /* jshint validthis: true */
    var vm = this;

    SheetmusicService.getComposers().success(function(data){
        vm.composers = data.results;
    });
}
