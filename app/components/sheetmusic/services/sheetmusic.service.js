'use strict';

/**
 * @ngdoc service
 * @name clientApp.SheetmusicService
 * @description
 * # SheetmusicService
 * Service in the clientApp.
 */

angular.module('clientApp')
  .factory('SheetmusicService', SheetmusicService);

function SheetmusicService($http, $resource) {

    var service = {},
        sheetmusicURL = '/api/sheetmusic/',
        composersURL = '/api/composers/',
        commentURL = '/api/comment/';

    service.getSheetmusic = function(sheetmusicId) {
        return $http.get( sheetmusicURL + sheetmusicId.toString() );
    };

    service.updateSheetmusic = function(sheetmusicId, data) {
        console.log(data);
        return $http.patch( sheetmusicURL + sheetmusicId.toString(), {'sheetmusic' : data});
    };

    service.getComposers = function(){
        return $http.get( composersURL );
    };

    service.getTopSheetmusic = function(){
        return $http.get( sheetmusicURL + 'top' );
    };

    service.getTrendingSheetmusic = function(days) {
        return $http({
            method: 'GET',
            url: sheetmusicURL + 'trending',
            params : {days : days, results: 10}
        }); 
    };

    service.getSheetmusicList = function(filterOptions) {
        return $http({
            method: 'GET',
            url: sheetmusicURL,
            params : filterOptions
        }); 
    };

    service.getRating = function(sheetId) {
        return $http({
            method: 'GET',
            url: sheetmusicURL + 'getrating',
            params : {sheet_id : sheetId}
        });
    };

    service.postRating = function(sheetId, value) {
        return $http.post(sheetmusicURL + 'rate/', {sheetmusic : sheetId, value: value});
    };

    service.getUploads = function() {
        return $http.get( sheetmusicURL + 'uploads/' );
    };

    service.deleteSheetmusic = function(sheetId) {
        return $http.delete( sheetmusicURL + sheetId );
    };

    service.postVideo = function(link, sheetID, title, grade){
        if (sheetID === '-') {
            return $http.post('/api/video/', {link: link, title: title, grade: grade});
        }
        else{
            return $http.post('/api/video/', {link: link, sheetmusicId : sheetID, title: title, grade: grade});
        }
    };

    service.search = function(query) {
        return $http.get('/api/search?query=' + query);
    };

    service.getComments = function(sheetmusicId) {
        return $http.get(commentURL + '?sheetmusicId=' + sheetmusicId);
    };

    service.addComment = function(commentText, sheetmusicId, recipientId) {
        return $http.post(commentURL, {commentText: commentText, sheetmusicId: sheetmusicId, recipientId: recipientId});
    };

    service.upvoteComment = function(commentId, sheetmusicId) {
        return $http.post('/api/commentupvote/', {commentId: commentId, sheetmusicId: sheetmusicId});
    };

    service.removeComment = function(commentId) {
        return $http.delete('/api/comment/' + commentId +'/');
    };

    service.editComment = function(commentId, commentText) {
        return $http.patch('/api/comment/' + commentId + '/', {commentText: commentText});
    };

    service.undoRemoveComment = function(commentId) {
        return $http.post('/api/comment/' + commentId + '/undodelete/');
    };

    return service;
}
