'use strict';

angular.module('clientApp')
  .service('MetaInformation', function() {

      var title = 'PianoShelf';
      var metaDescription = 'Piano sheet music';
      return {
        title: function() { return title; },
        setTitle: function(newTitle) { 
          title = newTitle; 
        },

        metaDescription: function() { return metaDescription; },
        setDescription: function(newMetaDescription) {
          metaDescription = newMetaDescription;
        },

        reset: function() {
          metaDescription = '';
          title = '';
        },
      };
  });
