'use strict';

angular.module('clientApp')
  .factory('AlertService', AlertService);

function AlertService($rootScope, $timeout) {
    var service = {};

    service.addAlert = function(message) {
        $rootScope.alerts.push({'msg' : message});
        $timeout( function(){ $rootScope.alerts.splice(0,1); }, 2000); // remove after 2 seconds
    };

    service.removeAlert = function(index) {
        $rootScope.alerts.splice(index, 1);
    };

    return service;
}