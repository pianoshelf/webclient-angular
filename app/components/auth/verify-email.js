'use strict';

angular.module('clientApp')
.controller('VerifyEmailCtrl', function ($state, $stateParams, AuthService){
    var vm = this;

    var key = $stateParams.key;
    vm.verified = false;

    console.log(key);

    AuthService.verifyEmail(key)
    .success(function(resp){
        vm.verified = true;
    })
    .error(function(resp){
        console.log(resp);
        $state.go('app.home');
    });

});
