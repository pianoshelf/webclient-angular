'use strict';

/**
 * Created by winston on 12/10/14.
 */

/* global gapi:false */
/* global FB:false */
/* global OAuth:false */

angular.module('clientApp')
.controller('LoginCtrl', function($scope, $state, $http, $cookies, $q, AuthService, loginService, AlertService){
      $scope.errorMsg = null;

    $scope.login = {
        working: false,
        wrong: false
    };

    function loginAction(loginPromise){
      $scope.login.working = true;
      $scope.login.wrong = false;

      loginService.loginUser(loginPromise);
      loginPromise.error(function (data) {
        $scope.login.wrong = true;
        $scope.errorMsg = data.non_field_errors.join('\n');

      });
      loginPromise.finally(function () {
        
        if (loginService.isLogged){
          $scope.login.working = false;
          $state.go('app.profile.shelf', { 'username' : loginService.user.username } );
        }
      });
    }

    $scope.login = function () {
      if ($scope.loginForm.$valid) {
        // setup promise, and 'working' flag
        var loginPromise = AuthService.login($scope.user);
        loginAction(loginPromise);

      } else if (!$scope.loginForm.email.$valid){
        $scope.errorMsg = 'Please enter an email.';

      } else if (!$scope.loginForm.password.$valid){
        $scope.errorMsg = 'Please enter a password.';
      }
    };

    $scope.facebooklogin = function () {
      fBLogin(AuthService.facebookLogin, function(promise) {
        loginAction(promise);
      });
    };

    function fBLogin(callback,res) {
        FB.init({
            appId: '1549195551980295',
            cookie: true, 
            xfbml: true, 
            status: true,
            version: 'v2.1' });
        
        FB.login(function(response) {
          if (response.status === 'connected') {
              response = response.authResponse;
              res(callback(response));
          } else if (response.status === 'not_authorized') {
            AlertService.addAlert('Not Authorized.');
          } else {
            AlertService.addAlert('Not signed in.');
          }
        });
    }

    $scope.twitterlogin = function () {
        OAuth.initialize('F75XX1od5f2-HnKRq_9T3DnT1jc'); // From oauth.io - Temporary until a client sided Twitter access-token pop-up flow is created
        OAuth.popup('twitter')
            .done(function(result) {
              var loginPromise = AuthService.twitterLogin(result);
              loginAction(loginPromise);
            })
            .fail(function (err) {
              console.log('Sign-in state: ' + err);
              AlertService.addAlert('Not signed in.');
        });
    };

    /* Google Plus - OAuth2
    $scope.googlelogin = function () {
       var additionalParams = {
         'clientid' : '572386280200-mn44knf4v91up8r4k17nrf93ger56m6u.apps.googleusercontent.com',
         'callback': googleSigninCallback
       };
        gapi.auth.signIn(additionalParams);
    };

    function googleSigninCallback(authResult) {
      if (authResult.status.signed_in) {
        var loginPromise = AuthService.googleLogin(authResult);
        loginAction(loginPromise);

      } else {
        // Possible error values:
        //   "user_signed_out" - User is signed-out
        //   "access_denied" - User denied access to your app
        //   "immediate_failed" - Could not automatically log in the user
        console.log('Sign-in state: ' + authResult.error);
      }
    }
    */

});
