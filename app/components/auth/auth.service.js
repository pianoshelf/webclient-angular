'use strict';

/**
 * @ngdoc service
 * @name clientApp.AuthService
 * @description
 * # AuthService
 * Service in the clientApp.
 */

angular.module('clientApp')
  .factory('AuthService', AuthService);

function AuthService($http, $resource) {

    var service = {},
        authURL = '/api-auth/';

    service.login = function(user) {
        return $http.post(authURL + 'login/', {
          username: user.email,
          password: user.password
        });
    };

    service.logout = function() {
        return $http.post(authURL + 'logout/');
    };

    service.verifyEmail = function(verification_key) {
      return $http.post(authURL + 'register/account-confirm-email/' + verification_key + '/');
    };

    service.register = function(user) {
        return $http.post(authURL + 'register/', {
          username: user.username,
          password1: user.password1,
          password2: user.password2,
          email: user.email
        });
    };

    // Facebook - Get access token
    service.facebookLogin = function(token) {
        return $http.post(authURL + 'social-login/facebook/', {
          access_token: token.accessToken
        });
    };

    /* Google Plus - get access token
    service.googleLogin = function(token) {
        return $http.post(authURL + 'social-login/google/', {
          access_token: token.access_token
        });
    };
    */

    // Twitter - Get access token
    service.twitterLogin = function(token) {
        return $http.post(authURL + 'social-login/twitter/', {
          access_token: token.oauth_token,
          access_token_secret: token.oauth_token_secret
        });
    };

    return service;
}
