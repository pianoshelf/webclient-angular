'use strict';

/**
 * Created by ryan on 22/10/14.
 */

angular.module('clientApp')
.controller('RegisterCtrl', function ($state, AuthService, loginService){

    var vm = this;
    vm.errorMsg = null;
    vm.register = register;

    function loginAction(loginPromise){
      loginService.loginUser(loginPromise);
      loginPromise.error(function (data) {
        vm.errorMsg = data.non_field_errors.join('\n');
      });
      loginPromise.finally(function () {
        $state.go('app.profile.shelf', { 'username' : loginService.user.username } );
      });
    }

    function register() {
      if (vm.registerForm.$valid) {
        // setup promise, and 'working' flag
        var registerPromise = AuthService.register(vm.user);

        registerPromise.error(function (data, res) {

            // Probably a better way
            if (data.username){
            vm.errorMsg = data.username[0];
          }
            else if (data.email){
            vm.errorMsg = data.email[0];
            }
            else if (data.password1){
            vm.errorMsg = data.password1[0];
            }
            else {
            vm.errorMsg = data.__all__[0];
            }
        });

        registerPromise.success(function () {

        vm.user.password = vm.user.password1;
        // Awks
        vm.user.email = vm.user.username;
        var loginPromise = AuthService.login(vm.user);
        loginAction(loginPromise);

        });
      }
      else{
        vm.errorMsg = 'Please fill out all the fields.';
      }
  }

});
