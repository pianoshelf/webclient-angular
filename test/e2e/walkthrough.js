/* browse around site */

describe('Walkthrough', function () {
  'use strict';

  var searchbar = element(by.model('searchText'));

  it('should be able to reach application at root url', function () {
    browser.get('/');

    // maximize browser so that it uses the large screen UI
    browser.driver.manage().window().maximize();
  });

  describe('create an user account on the homepage', function () {
    beforeEach(function () {
      browser.get('/');
    });

    it('registers and sign-in a new user with valid information', function () {
      var name = element(by.model('user.username')),
          email = element(by.model('user.email')),
          password = element(by.model('user.password1')),
          passconf = element(by.model('user.password2'));

      name.sendKeys("John Doe");
      email.sendKeys("test@pianoshelf.com");
      password.sendKeys(123456);
      passconf.sendKeys(123456);

      // not sure if it actually picks up the button since it currently does nothing..
      element.all(by.css('.register-form .form-group input.btn-primary')).first().click();
      browser.sleep(1000);
    });
  });

  describe('search some music', function () {
    // search bar should be available on every page anyways

    it('search popular pieces', function () {
      searchbar.sendKeys("The Four Seasons");

      // do some validation on query correctness
    });
  });

  describe('look at some sheet musics', function () {

    beforeEach(function () {
      browser.get('sheetmusic');
    });

    it('do some browsing', function () {
      // select first musician
      element(by.css('li.media:nth-child(1) > a:nth-child(1)')).click();
      // select first music piece
      element(by.css('tr.ng-scope:nth-child(1) > td:nth-child(1) > a:nth-child(1)')).click();

      searchbar.sendKeys("Nice Right?");
      searchbar.clear();
      searchbar.sendKeys("Awesome!");
      browser.sleep(2000);
    });
  });
});
