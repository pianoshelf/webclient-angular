exports.config = {
  specs: ['e2e/*.js'],
  capabilities: {
    'browserName': 'firefox'
  },
  chromeOnly: false,
  baseUrl: 'http://localhost:9900/'
};
